#pragma once

#include "Order.h"
#include "Instruction.h"
#include "T.h"
//#include "Header.h"
//#include <string>
#include <vector>
#include <map>
#include <iostream>
using namespace std;
static const int number_of_ranks = 9;
static const int number_of_status = 4;
static const int number_of_spec = 8;
static const int number_of_posts = 1;
static const string rank_list[number_of_ranks] = { "Private", "Sgt.", "Lt.", "Capt.", "Maj.", "Lt. - Col.", "Col.", "Gen.", "Marshal" };
static const string status_list[number_of_status] = { "healthy", "injured", "killed", "missing" };
static const string specialization_list[number_of_spec] = { "infantry", "special_forces", "scout", "sharpshooter", "sapper", "NBC", "medical", "radioman" };
static const string post_list[number_of_posts] = { "officier" };
//!Basic Abstract class
class Man
{
protected:
	string name; 
	int rank;
	int size;
	int health;
public:
	/*!����������� �� ���������*/
	Man(){};
	/*!�����������
	\param [in] n ���
	\param [in] r ������
	\param [in] s ������ ������
	\param [in] h ���������(������/�����/����/������ ��� �����)*/
	Man(string n, int r, int s, int h);
	/*!
	\return  name ���*/
	string getName() const{ return name; };
	/*!
	\return ������*/
	string getRank() const{ return rank_list[rank]; };
	/*!
	\return  ������*/
	int getRanknumber() const { return rank; };
	/*!
	\return ���������*/
	int getHealthnumber() const { return health; };
	/*!
	\return ������ ������*/
	int getSize() const{ return size; };
	/*!
	\return  ���������*/
	string getHealth() const{ return status_list[health]; };
	/*!������������� ��� �������
	\param [in]  newName ���*/
	Man& setName(string newName){ name = newName;  return *this; };
	/*!������������� ������ �������
	\param [in] newRank ������*/
	Man& setRank(int newRank);
	/*!���������� ������ ������ �������
	\param [in] newSize ������ ������*/
	Man& setSize(int newSize){ size = newSize;  return *this; };
	/*!������������� ��������� �������
	\param [in] newHealth  ���������*/
	Man& setHealth(int newHealth);
	/*!�������� ��������� ������� � ����������� �� ������� ����� � �������������
	\param [in] h  ���������*/
	Man& changeHealth(int h);
	/*!
	\return  ��� �������(������\������� ������\������� ��������\�����������)*/
	virtual int iAm()const = 0;
	/*!
	\return  ��������� �� �����-�����*/
	virtual Man* clone() const = 0;
	//!����������
	virtual ~Man(){};
};
//!Class Soldier
class Soldier :virtual public Man{
protected:
	int specialization;
	Order *order;
public:
	/*!����������� �� ���������*/
	Soldier(){};
	/*!�����������
	\param [in] n ���
	\param [in] r ������
	\param [in] s ������ ������
	\param [in] h ���������(������/�����/����/������ ��� �����)
	\param [in] spec �������������*/
	Soldier(string n, int r, int s, int h, int spec);
	/*!�����������
	\param [in] n ���
	\param [in] r ������
	\param [in] s ������ ������
	\param [in] h ���������(������/�����/����/������ ��� �����)
	\param [in] spec �������������
	\param [in] *order ������ ��������*/
	Soldier(string n, int r, int s, int h, int spec, Order *order);


	/*!
	\return �������������*/
	string getSpec() const{ return specialization_list[specialization]; };
	/*!
	������������� ������������� �������
	\param [in] spec ����� �������������*/
	Soldier& setSpec(int spec);
	/*!
	\return  �������������*/
	int getSpecnumber() const{ return specialization; };
	/*!������������� �������� ������
	\param [in] s �������� �����
	\param [in] &sol ������ �� ����� Soldier
	\return ��������� ������*/
	friend ostream & operator << (ostream &, const Soldier &);
	/*!������������� �������� �����
	\param [in] s ������� �����
	\param [in] &sol ������ �� ����� Soldier
	\return ��������� ������*/
	friend istream & operator >> (istream&, const Soldier&);
	/*!
	\return  ��� �������(������\������� ������\������� ��������\�����������)*/
	virtual int iAm()const { return 1; }
	/*!
	\return  ��������� �� �����-�����*/
	virtual Man* clone() const{ return new Soldier(*this); }
	//!Virtual destructor
	virtual ~Soldier(){};
};
//!Class Staff_officier
class Staff_officier :virtual public Man{
protected:
	int post;
	Instruction *instruction;
public:
	//!Constructor
	Staff_officier(string n, int r, int s, int h, int p);
	//!Constructor
	Staff_officier(string n, int r, int s, int h, int p, Instruction *inst);
	//!Getting post
	string getPost() const{ return post_list[post]; };
	//!Setting post
	Staff_officier& setPost(int p);
	//!Output
	friend ostream & operator << (ostream &, const Staff_officier &);
	//!Virtual iAm method
	virtual int iAm()const { return 2; }
	//!Virtual Clone Method
	virtual Man* clone() const{ return new Staff_officier(*this); }
	//!Virtual destructor
	virtual ~Staff_officier(){};
};
//!Class Staff
class Staff
{
protected:
	marray < int, Man*> *table;
	string place;
public:
	//!Basic constructor
	Staff(){};
	//!Constructor
	Staff(marray <int, Man*> *t, string p){ table = t; place = p; };
	//!Getting table
	marray<int, Man*>getTable() const { return *table; }
	//!Setting table
	Staff& setTable(marray < int, Man*> *t){ table = t; return *this; };
	//!Getting place
	string getPlace() const { return place; }
	//!Adding new item
	void addMan(int, Man *);
	//!Transfer soldier from onedivision to another
	void Transfer(int, int, int, int);
	//!Delete Man from table
	Staff& deleteMan(int);
	//!Finding Man
	Man* findMan(int);
	//!Clone
	Staff* clone() const{ return new Staff(*this); };
	//!Output
	friend ostream & operator << (ostream &s, const Staff &st);
	//!Virtual destructor
	virtual ~Staff(){};
};
//!Class Division
class Division : public Staff{
public:
	//!Basic constructor
	Division(){};
	//!Constructor
	Division(marray <int, Man*> *t, string p) :Staff(t, p){};
	//!Changing place
	Division& changePlace(string){ return *this; };
	//!Clone
	Division* clone() const{ return new Division(*this); };
	//!Repair
	void repair(Man *m, Division&d);
	//!Split division
	void Split(Staff&st, Man* m);
	//!Virtual destructor
	virtual ~Division(){};
};
//!Class Commander
class Commander : public Staff_officier{
protected:
	Staff &staff;
public:
	//!Constructor
	Commander(string n, int r, int s, int h, int p, Staff& d) :Man(n, r, s, h), Staff_officier(n, r, s, h, p), staff(d){};
	//!Constructor
	Commander(string n, int r, int s, int h, int p, Instruction *inst, Staff& d) :Man(n, r, s, h), Staff_officier(n, r, s, h, p, inst), staff(d){};
	//!Output
	friend ostream & operator << (ostream &, const Commander &);
	//!Getting staff
	Staff& getStaff(){ return staff; }
	//!Setting staff
	Commander& setStaff(Staff st){ staff = st; return *this; };
	//!=
	Commander & Commander::operator = (Commander&);
	//!Virtual method iAm
	virtual int iAm()const { return 4; }
	//!Virtual CloneMethod
	virtual Man* clone() const{ return new Commander(*this); }
	//!Virtual destructor
	virtual ~Commander(){};
};

//!Class Field_officier
class Field_officier : public  Staff_officier, public Soldier{
protected:
	Division *div;
public:
	//!Constructor
	Field_officier(string n, int r, int s, int h, int p, int spec, Division* d) :Man(n, r, s, h), Staff_officier(n, r, s, h, p), Soldier(n, r, s, h, spec), div(d){};
	//!Constructor
	Field_officier(string n, int r, int s, int h, int p, int spec, Instruction *inst, Division* d) :Man(n, r, s, h), Staff_officier(n, r, s, h, p, inst), Soldier(n, r, s, h, spec), div(d){};
	//!Output
	friend ostream & operator << (ostream &, const Field_officier &);
	//!Setting Division
	Field_officier& setDivision(Division* d){ div = d; return *this; }
	//!Gettinr Division
	Division* getDivision(){ return div; }
	//!Virtual method iAm
	virtual int iAm()const { return 3; }
	//!Virtual CloneMethod
	virtual Man* clone() const{ return new Field_officier(*this); }
	//!Virtual destructor
	virtual ~Field_officier(){};
};
//!Class Descriptor, describes learnings
class Descriptor
{
protected:
	Commander &com1;
	Commander &com2;
	string place;
	double time;
public:
	//!Getting first Commander
	Commander& get1Commander(){ return com1; };
	//!Getting second Commander
	Commander& get2Commander(){ return com2; };
	//!=
	Descriptor & Descriptor::operator = (Descriptor&);
	//!Constructor
	Descriptor(Commander& c1, Commander& c2, string p, double t) : com1(c1), com2(c2), place(p), time(t){};
	//!Virtual destructor
	~Descriptor(){};
};