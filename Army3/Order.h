#pragma once
#include <string>
using namespace std;
//!Class order, includes orders
class Order
{
protected:
	string order;
public:
	Order();
	string getOrder() const;
	Order& setOrder(string);
	~Order();
};

