#include "stdafx.h"
#include "Man.h"



Man::Man(string n, int r, int s, int h){
	if (r < number_of_ranks && h < number_of_status){
		name = n;
		rank = r;
		size = s;
		health = h;
	}
	else
		throw(invalid_argument("incorrect rank or status in constructor"));
}

Man& Man::setRank(int newRank){
	if (newRank < number_of_ranks)
		rank = newRank;
	else
		throw(invalid_argument("incorrect rank entered"));
	return *this;
};

Man& Man::setHealth(int newHealth){
	if (newHealth < number_of_status)
		health = newHealth;
	else
		throw(invalid_argument("incorrect status entered"));
	return *this;
};

Man& Man::changeHealth(int h){
	if (h < number_of_status){
		if (health == 0)
			health = h;
		if (health == 1)
			if (h != 0)
				health = h;
	}
	else
		throw(invalid_argument("incorrect status entered"));
	return *this;
}

Soldier::Soldier(string n, int r, int s, int h, int spec) : Man(n, r, s, h){
	if (spec < number_of_spec){
		specialization = spec;
		order = NULL;
	}
	else
		throw(invalid_argument("incorrect specialization in constructor"));
}

Soldier::Soldier(string n, int r, int s, int h, int spec, Order *o) : Man(n, r, s, h){
	if (spec < number_of_spec){
		specialization = spec;
		order = o;
	}
	else
		throw(invalid_argument("incorrect specialization in constructor"));

}

Soldier& Soldier::setSpec(int spec){
	if (spec < number_of_spec)
		specialization = spec;
	else
		throw(invalid_argument("incorrect specialization entered"));
	return *this;
}


Staff_officier::Staff_officier(string n, int r, int s, int h, int p) : Man(n, r, s, h){
	if (p < number_of_posts){
		post = p;
		instruction = NULL;
	}
	else
		throw(invalid_argument("incorrect post in constructor"));
}

Staff_officier::Staff_officier(string n, int r, int s, int h, int p, Instruction *inst) : Man(n, r, s, h){
	if (p < number_of_posts){
		post = p;
		instruction = inst;
	}
	else
		throw(invalid_argument("incorrect post in constructor"));
}

Staff_officier& Staff_officier::setPost(int p){
	if (p < number_of_posts)
		post = p;
	else
		throw(invalid_argument("incorrect post entered"));
	return *this;
}

ostream & operator << (ostream &s, const Soldier &sol)
{
	s << "    " << sol.getName();
	s << "    " << sol.getRank();
	s << "    " << sol.getSize();
	s << "    " << sol.getSpec();
	s << "    " << sol.getHealth();
	return s;
}

ostream & operator << (ostream &s, const Staff_officier &st){
	s << "  " << st.getName() << endl;
	s << "  " << st.getRank() << endl;
	s << "  " << st.getSize() << endl;
	s << "  " << st.getPost() << endl;
	s << "  " << st.getHealth() << endl;
	return s;
}

ostream & operator << (ostream &s, const Commander &com){
	s << com.getName() << endl;
	s << com.getRank() << endl;
	s << com.getSize() << endl;
	s << com.getPost() << endl;
	s << com.getHealth() << endl;
	return s;
}

ostream & operator << (ostream &s, const Field_officier &com){
	s << com.getName() << endl;
	s << com.getRank() << endl;
	s << com.getSize() << endl;
	s << com.getPost() << endl;
	s << com.getSpec() << endl;
	s << com.getHealth() << endl;
	return s;
}

ostream & operator << (ostream &s, const Staff &st){
	for (marray<int, Man*>::Iterator it = st.getTable().begin(); it < st.getTable().end(); it++){
		s << it->key << " = " << it->data << std::endl;
		if (it->data->iAm() == 3){
			Field_officier *a = dynamic_cast<Field_officier*>(it->data);
			for (marray<int, Man*>::Iterator it = a->getDivision()->getTable().begin(); it < a->getDivision()->getTable().end(); it++)
				s << it->key << " = " << it->data << std::endl;
		}
		return s;
	}
}

void Staff::addMan(int n, Man* m){
	//table[n] = m->clone();
	table->add(n, m->clone());
	/*map<int, Man*>::iterator p = table.find(n);
	if (p == table.end()){
		pair<map<int, Man*>::iterator, bool>pp = table.insert(make_pair(n, m->clone()));
		if (!pp.second)
			throw std::exception("Can't insert item");
	}
	else{
		delete p->second;
		p->second = m->clone();
	}*/
}


Staff & Staff::deleteMan(int n){
	if (table->find(n) != NULL)
		table->remove(n);

	/*auto it = table.find(n);
	if (it != table.end()) {
		table.erase(n);
	}*/
	else
		throw(invalid_argument("Can't find this number"));
	return *this;
}
Man* Staff::findMan(int n){ //������������� ����� � �������� � ��������� � ���������� ���������
	Man *p = table->find(n);
	if (p != NULL){
		return table->find(n);
	}
	else {
		throw(invalid_argument("Can't find this number"));
		return NULL;
	}
	//return table.find(n)->second;//����� ������������� � ���� ������� ��� �� ��� ����������
}

void Division::repair(Man *m, Division &d){
	int flag = 0;
	marray<int, Man*>::Iterator it;
	for (it = d.getTable().begin(); it < d.getTable().end(); it++){
	//for (int i = 0; i < d.getTable().size(); i++){
		//int iam = d.getTable().valueAt(i)->iAm();
		int iam = it->data->iAm();
		//Soldier *s = dynamic_cast<Soldier*>(d.getTable().valueAt(i));
		Soldier *s = dynamic_cast<Soldier*>(it->data);
		if (s->getSpec() == "medical")
			flag = 1;
	}
	if ((m->getHealth() == "injured") && (flag == 1))
		m->setHealth(0);
}
void Division::Split(Staff &st, Man *m){
	Field_officier *fo = dynamic_cast<Field_officier*>(m);
	marray <int, Man*> t = fo->getDivision()->getTable();
	Division *d = fo->getDivision();
	int n[10];
	int MaxRank = 0;
	for (int i = 0; i < t.size(); i++){
		if (t.valueAt(i)->getRanknumber() > MaxRank)
			MaxRank = t.valueAt(i)->getRanknumber();
	}
	int k = 0;
	int sol = 0;
	for (int i = 0; i < t.size(); i++){
		if (t.valueAt(i)->getRanknumber() == MaxRank){
			Soldier *s = dynamic_cast<Soldier*>(t.valueAt(i));
			Division sta;
			Field_officier fol(s->getName(), s->getRanknumber(), s->getSize(), s->getHealthnumber(), 0, s->getSpecnumber(), sta.clone());
			Man *m = dynamic_cast<Man*>(&fol);
			st.addMan(t.keyAt(i), m->clone());
			fo->getDivision()->deleteMan(t.keyAt(i));
			n[k] = t.keyAt(i);
			k++;
		}
		else
			sol++;
	}
	cout << k;
	cout << sol;
	
	int delit = sol / (k + 1);

	for (int i = 0; i < k; i++){
		Field_officier *f = dynamic_cast<Field_officier*>(st.findMan(n[i]));
		for (int j = 0; j < delit; j++){
			marray <int, Man*> t = fo->getDivision()->getTable();
			Man *m = dynamic_cast<Man*>(t.valueAt(i));
			f->getDivision()->addMan(t.keyAt(i), m->clone());
			fo->getDivision()->deleteMan(t.keyAt(i));
		}
	}
	
}

Descriptor & Descriptor::operator=(Descriptor &d){
	com1 = d.com1;
	com2 = d.com2;
	place = d.place;
	time = d.time;
	return *this;
}

Commander & Commander::operator =(Commander&d){
	name = d.name;
	health = d.health;
	instruction = d.instruction;
	post = d.post;
	rank = d.rank;
	size = d.size;
	staff = d.staff;
	return *this;
}