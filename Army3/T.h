
#include "stdafx.h"
#include <iostream>
#include <string>
#include <new>
#include <assert.h>

template<typename KEY, class T>
//!Assotiative array
class marray {
	struct info {
		KEY key;
		T   data;
	};
private:
	info *arr;
	int  cnt;
	int  mem;
public:
	marray() :arr(NULL),
		cnt(0),
		mem(16){}
	~marray(){}
public:
	//!Iterator
	typedef info* Iterator;//!const iterator
	typedef const info* Iterator_const;//!First element
	Iterator begin(){ return arr; }//!First element
	Iterator_const begin() const { return arr; }//!Last element
	Iterator end(){ return arr + size(); }//!Last element
	Iterator_const end() const { return arr + size(); }
	//!Clone array
	marray* clone() const{ return new marray(*this); };

	//!Adding element KEY = VALUE
	T add(const KEY& key, const T& data){
		int i = _bin_find(key);
		if (!_heap_alloc_())
			return NULL;
		if (key == arr[i].key)
			return NULL;

		if (i < cnt){
			info* pb = arr + cnt;
			info* pa = arr + i;
			for (; pb > pa; --pb)
				*pb = *(pb - 1);
		}
		arr[i].key = key;
		arr[i].data = data;
		++cnt;
		return arr[i].data;
	}
	//!Delete element
	void remove(const KEY& key){
		int i = _bfind_info(key);
		if (i != -1){
			--cnt;
			info* pa = arr + i;
			info* pb = arr + cnt;
			for (; pa < pb; ++pa)
				*pa = *(pa + 1);

			if (!cnt)
				this->clear();
		}
	}

	//!Find VALUE with KEY O(log(n))
	T find(const KEY& key){
		int i = _bfind_info(key);
		return (i != -1) ? arr[i].data : NULL;
	}
	//!Find VALUE with KEY O(log(n)) const
	const T find(const KEY& key) const {
		const int i = _bfind_info(key);
		return (i != -1) ? arr[i].data : NULL;
	}
	//!IsEmpty
	bool empty() const {
		return (arr == NULL);
	}

	//!Number of elements
	int size() const {
		return cnt;
	}

	//!Clearing array
	void clear(){
		if (arr != NULL)
			delete[] arr;
		arr = NULL;
		cnt = 0;
		mem = 16;
	}

	//!Getting KEY with index
	const KEY& keyAt(int i) const {
		return arr[i].key;
	}

	//!Getting VALUE with index
	T& valueAt(int i) const {
		return arr[i].data;
	}

	//! Operator [] for array
	const T& operator [] (const KEY& key) const {
		const int i = _bfind_info(key);

		assert(i != -1);
		return arr[i].data;
	}
	//! Operator [] for array const
	T& operator [] (const KEY& key){
		int i = _bin_find(key);
		if ((i < cnt) && (key == arr[i].key))
			return arr[i].data;

		if (_heap_alloc_()){
			if (i < cnt){
				info* pb = arr + cnt;
				info* pa = arr + i;
				for (; pb > pa; --pb)
					*pb = *(pb - 1);
			}
			arr[i].key = key;
			arr[i].data = T();
			++cnt;
		}
		else
			assert(false);

		return arr[i].data;
	}

private:

	//!Allocation
	bool _heap_alloc_(){
		if (arr == NULL){
			arr = new (std::nothrow) info[mem];
			if (arr == NULL)
				return false;
		}
		else if ((cnt + 1) >= mem){
			int  tmem = cnt + mem / 2;
			info* tmp = new (std::nothrow) info[tmem];
			if (tmp == NULL)
				return false;

			const info* p = arr;
			const info* e = arr + cnt;
			info* d = tmp;
			while (p != e)
				*d++ = *p++;

			delete[] arr;
			arr = tmp;
			mem = tmem;
		}
		return true;
	}

	//!Finding KEY to put some element
	int _bin_find(const KEY& key){
		if (!cnt || (key < arr[0].key))
			return 0;
		else if (arr[cnt - 1].key < key)
			return cnt;

		int m = 0, f = 0, l = cnt;

		while (f < l){
			m = (f + l) >> 1;
			if (key < arr[m].key)
				l = m;
			else {
				if (key == arr[m].key)
					return m;
				f = m + 1;
			}
		}
		return f;
	}

	//!Finding KEY
	int _bfind_info(const KEY& key) const {
		if (!cnt || (key < arr[0].key))
			return -1;
		else if (arr[cnt - 1].key < key)
			return -1;

		int m = 0, f = 0, l = cnt;

		while (f < l){
			m = (f + l) >> 1;
			if (key < arr[m].key)
				l = m;
			else {
				if (key == arr[m].key)
					return m;
				f = m + 1;
			}
		}
		return -1;
	}
};