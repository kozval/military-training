#pragma once
#include <string>
using namespace std;
//!Class instructions, includes instructions for staff_officiers
class Instruction
{
protected:
	string instruction;

public:
	Instruction();
	string getInstruction() const;
	Instruction& setInstruction(string);
	~Instruction();
};

