
#include "stdafx.h"
#include "..\Army3\Man.h"

const char *msgs[] = {
	"0. Quit",
	"1. Add soldier",
	"2. Add staff_officier",
	"3. Add field_officier",
	"4. Correct fighter",
	"5. Delete officier",
	"6. Delete soldier",
	"7. Show info",
	"8. Transfer Soldier from one division to another",
	"9. Find number of officiers and soldiers with rank 'n'",
	"10. Split" };
const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);

const char *msgsAdd[] = {
	"1. Add Soldier to division",
	"2. Add Staff_officier to staff",
	"3. Add Field_officier to staff" };
const int NMsgsAdd = sizeof(msgsAdd) / sizeof(msgsAdd[0]);

int dialog(const char *msgs[], int);
int D_add_soldier(Descriptor&);
int D_add_staff_officier(Descriptor&);
int D_add_field_officier(Descriptor&);
int D_correct(Descriptor&);
int D_delete_officier(Descriptor&);
int D_delete_soldier(Descriptor&);
int D_find_rank(Descriptor&);

int D_show(Descriptor&);
int D_transfer(Descriptor&);
int D_split(Descriptor&);
int enterNum(int &);
void output(map<int, Man&>);
Soldier getSoldier();

int(*fptr[])(Descriptor&) = { NULL, D_add_soldier, D_add_staff_officier, D_add_field_officier, D_correct, D_delete_officier, D_delete_soldier, D_show, D_transfer, D_find_rank, D_split };

int enterNum(int &a)
{
	cin >> a;
	if (!cin.good())
		return -1;
	return 1;
}

int getRank(){
	int rank;
	cout << "Enter rank:   ";
	cin >> rank;
	return rank;
}
int getSize(){
	int size;
	cout << "Enter size:   ";
	cin >> size;
	return size;
}
int getSpec(){
	int spec;
	cout << "Enter specialization:   ";
	cin >> spec;
	return spec;
}
int getPost(){
	int post;
	cout << "Enter post:   ";
	cin >> post;
	return post;
}
string getName(){
	string h;
	cout << "Enter name:   ";
	cin >> h;
	return h;
}
int getNumber(){
	int n;
	cout << "Enter number in table:   ";
	cin >> n;
	return n;
}
string getPosition(){
	string p;
	cout << "Enter position of Staff/Division:   ";
	cin >> p;
	return p;
}
int getHealth(){
	int n;
	cout << "Enter health:   ";
	cin >> n;
	return n;
}

Soldier getSoldier(){
	string name;
	int rank;
	int size;
	int health;
	int spec;

	cout << "Enter soldier" << endl;
	cout << "Enter name:   ";
	cin >> name;
	cout << "Enter rank:   ";
	cin >> rank;
	cout << "Enter size:   ";
	cin >> size;
	cout << "Enter health:  ";
	cin >> health;
	cout << "Enter specialization:   " << endl;
	cin >> spec;
	Soldier s(name, rank, size, health, spec);
	return s;
}

Staff_officier add_Staff_officier(){
	cout << "Enter staff_officier info" << endl;
	string name = getName();
	int rank = getRank();
	int health = getHealth();
	int size = getSize();
	int post = getPost();
	Staff_officier s(name, rank, size, health, post);
	return s;

}

void output(marray<int, Man*> tab){
	for (marray<int, Man*>::Iterator it = tab.begin(); it < tab.end(); it++){
		int iam = it->data->iAm();
		if (iam == 1){
			cout  << it->key << ":";
			Soldier *s = dynamic_cast<Soldier*>(it->data);
			cout << *s << endl;
		}
		if (iam == 2){
			cout << endl;
			cout  <<"   "<< it->key << ":";
			Staff_officier *s = dynamic_cast<Staff_officier*>(it->data);
			cout << *s << endl;
		}
		if (iam == 3){
			cout << endl;
			cout << it->key << ":";
			Field_officier *s = dynamic_cast<Field_officier*>(it->data);
			cout << *s << endl;
			output(*s->getDivision()->getTable().clone());
		}
	}
}

int find_rank(marray<int, Man*> tab, int rank){
	int k = 0;
	for (int i = 0; i < tab.size(); i++){

		int iam = tab.valueAt(i)->iAm();
		if (iam == 1){
			Soldier *s = dynamic_cast<Soldier*>(tab.valueAt(i));
			if (rank == s->getRanknumber())
				k++;
		}
		if (iam == 2){
			Staff_officier *s = dynamic_cast<Staff_officier*>(tab.valueAt(i));
			if (rank == s->getRanknumber())
				k++;
		}
		if (iam == 3){
			Field_officier *s = dynamic_cast<Field_officier*>(tab.valueAt(i));
			if (rank == s->getRanknumber())
				k++;
			k += find_rank(s->getDivision()->getTable(), rank);
		}
	}
	return k;
}


void outputSoldier(marray<int, Man&> tab){
	for (int i = 0; i < tab.size(); i++){
		cout << tab.keyAt(i) << ":" << endl;
		Soldier &s = dynamic_cast<Soldier&>(tab.valueAt(i));
		cout << s << endl;
	}
}

Descriptor init(){
	
	Soldier s("Ivan", 3, 50, 0, 5);
	Man *m = dynamic_cast<Man*>(&s);
	marray <int, Man*> t;
	t.add(12, m);
	Division d(&t, "siria");
	Field_officier fo("Kolya", 5, 60, 0, 0, 6, &d);
	Man *m1 = dynamic_cast<Man*>(&fo);
	marray <int, Man*> t1;
	t1.add(3, m1);
	Staff st(&t1, "moscow");
	Commander a("Putin", 7, 46, 0, 0, st);

	Soldier s1("John", 2, 60, 0, 3);
	Man *m2 = dynamic_cast<Man*>(&s1);
	marray <int, Man*> t2;
	t2.add(6, m2);
	Division d1(&t2, "Afganistan");            //������ �������;
	Field_officier fo1("Jack", 4, 80, 0, 0, 7, &d1);
	Man *m3 = dynamic_cast<Man*>(&fo1);
	marray <int, Man*> t3;
	t3.add(38, m3);
	Staff st1(&t3, "washington");
	Commander b("obama", 6, 30, 0, 0, st1);

	Descriptor des(a, b, "EveryWhere", 3.5);


	return des;
}





int dialog(const char *msgs[], int N)
{
	char *errmsg = "";
	int rc;
	int i, n;
	do{
		cout << errmsg << endl;
		errmsg = "You are wrong. Repeat, please!";
		for (i = 0; i < N; ++i)
			cout << msgs[i] << endl;
		cout << endl;
		cout << "Make your choice ---> ";
		n = enterNum(rc);
		if (n <= 0)
			rc = 0;
	} while (rc < 0 || rc >= N);
	return rc;
}

int _tmain(int argc, _TCHAR* argv[])
{
	Soldier s("Ivan", 3, 50, 0, 5);
	Man *m = dynamic_cast<Man*>(&s);
	marray <int, Man*> t;
	//t[0] = m;
	t.add(0, m);
	cout << t.keyAt(0)<<std::endl;
	Soldier *ss = dynamic_cast<Soldier*>(t.valueAt(0));
	cout << *ss;//���������!!!
	Division d(&t, "siria");//���� � �����
	Field_officier fo("Kolya", 5, 60, 0, 0, 6, &d);
	Man *m1 = dynamic_cast<Man*>(&fo);
	marray <int, Man*> t1;
	t1.add(3, m1);
	Staff st(&t1, "moscow");
	Commander a("Putin", 7, 46, 0, 0, st);

	Soldier s1("John", 2, 60, 0, 3);
	Man *m2 = dynamic_cast<Man*>(&s1);
	marray <int, Man*> t2;
	t2.add(6, m2);
	Division d1(&t2, "Afganistan");            //������ �������;
	Field_officier fo1("Jack", 4, 80, 0, 0, 7, &d1);
	Man *m3 = dynamic_cast<Man*>(&fo1);
	marray <int, Man*> t3;
	t3.add(38, m3);
	Staff st1(&t3, "washington");
	Commander b("obama", 6, 30, 0, 0, st1);

	Descriptor des(a, b, "EveryWhere", 3.5);



	Commander a1 = des.get1Commander();
	cout << a1 << endl;
	Commander b1 = des.get2Commander();
	cout << b1 << endl;
	marray<int, Man*> ta = a1.getStaff().getTable();
	marray<int, Man*> tb = b1.getStaff().getTable();
	output(a1.getStaff().getTable()); //error in soldier output recursion
	output(b1.getStaff().getTable());


	int rc;
	while (rc = dialog(msgs, NMsgs))
		if (!fptr[rc](des))
			break;

	cout << "That's all" << endl;
	system("pause");
	return 0;
}

int D_add_soldier(Descriptor& d){
	Soldier s = getSoldier();
	Man *m = dynamic_cast<Man*>(&s);
	cout << "Enter number of commander, 1 or 2" << endl;
	int n1;
	cin >> n1;
	cout << "Enter number of Field_commander of division to put soldier";
	int n2;
	cin >> n2;
	int number = getNumber();
	int iam = 0;
	if (n1 == 1)
		iam = d.get1Commander().getStaff().findMan(n2)->iAm();
	if (n1 == 2)
		iam = d.get2Commander().getStaff().findMan(n2)->iAm();

	if (iam == 3){
		if (n1 == 1){
			Field_officier *fo = dynamic_cast<Field_officier*>(d.get1Commander().getStaff().findMan(n2));
			fo->getDivision()->addMan(number, m);
		}
		if (n1 == 2){
			Field_officier *fo = dynamic_cast<Field_officier*>(d.get2Commander().getStaff().findMan(n2));
			fo->getDivision()->addMan(number, m);
		}
	}
	else{
		throw(invalid_argument("Incorrect number"));
	}
	return 1;
};
int D_add_field_officier(Descriptor& d){
	marray <int, Man*> t;
	Division st(t.clone(),"");
	int number;
	
	cout << "Enter field_officier info" << endl;
	string name = getName();
	int rank = getRank();
	int health = getHealth();
	int size = getSize();
	int post = getPost();
	int spec = getSpec();
	number = getNumber();
	Field_officier fo(name, rank, size, health, post, spec, st.clone());
	cout << "Enter number of commander, 1 or 2" << endl;
	int n1;
	cin >> n1;
	if (n1 == 1)
		d.get1Commander().getStaff().addMan(number, &fo);
	if (n1 == 2)
		d.get2Commander().getStaff().addMan(number, &fo);
	return 1;
}
int D_add_staff_officier(Descriptor& d){
	Staff_officier s = add_Staff_officier();
	int number = getNumber();
	int num;
	cout << "Enter number of commander, 1 or 2" << endl;
	cin >> num;
	if (num == 1){
		Man *m = dynamic_cast<Man*>(&s);
		d.get1Commander().getStaff().addMan(number, m);
	}
	if (num == 2){
		Man *m = dynamic_cast<Man*>(&s);
		d.get2Commander().getStaff().addMan(number, m);
	}
	return 1;
}
int D_correct(Descriptor& d){
	int num;
	cout << "Enter number of commander, 1 or 2" << endl;
	cin >> num;
	cout << "Enter number of Field Officier of Staff Officier" << endl;
	return 1;
};
int D_delete_officier(Descriptor& d){
	int num;
	cout << "Enter number of commander, 1 or 2" << endl;
	cin >> num;
	cout << "Enter number of Field Officier to delete his soldier" << endl;
	int number = getNumber();
	if (num == 1){
		d.get1Commander().getStaff().deleteMan(number);
	}
	if (num == 2){
		d.get2Commander().getStaff().deleteMan(number);
	}
	return 1;
};
int D_delete_soldier(Descriptor& d){
	int n1;
	cout << "Enter number of commander, 1 or 2" << endl;
	cin >> n1;
	cout << "Enter number of Field Officier to delete his soldier" << endl;
	int n2 = getNumber();
	cout << "Enter number of soldier" << endl;
	int n3 = getNumber();
	int iam = 0;
	if (n1 == 1)
		iam = d.get1Commander().getStaff().findMan(n2)->iAm();
	if (n1 == 2)
		iam = d.get2Commander().getStaff().findMan(n2)->iAm();
	if (iam == 3){
		if (n1 == 1){
			Field_officier *fo = dynamic_cast<Field_officier*>(d.get1Commander().getStaff().findMan(n2));
			fo->getDivision()->deleteMan(n3);
		}
		if (n1 == 2){
			Field_officier *fo = dynamic_cast<Field_officier*>(d.get2Commander().getStaff().findMan(n2));
			fo->getDivision()->deleteMan(n3);
		}
	}
	else{
		throw(invalid_argument("Incorrect number"));
	}

	return 1;
}
int D_show(Descriptor& d){
	cout << d.get1Commander();
	output(d.get1Commander().getStaff().getTable());
	cout << d.get2Commander();
	output(d.get2Commander().getStaff().getTable());
	return 1;
};
int D_transfer(Descriptor& d){
	Man *m = nullptr;
	cout << "Enter number of commander, 1 or 2" << endl;
	int n1;
	cin >> n1;
	cout << "Enter number of Field_commander of division to transfer soldier out";
	int n2;
	cin >> n2;
	cout << "Enter number of soldier ";
	int n3;
	cin >> n3;
	int iam = 0;
	if (n1 == 1)
		iam = d.get1Commander().getStaff().findMan(n2)->iAm();
	if (n1 == 2)
		iam = d.get2Commander().getStaff().findMan(n2)->iAm();
	if (iam == 3){
		if (n1 == 1){
			Field_officier *fo = dynamic_cast<Field_officier*>(d.get1Commander().getStaff().findMan(n2));
			m = fo->getDivision()->findMan(n3)->clone();
			fo->getDivision()->deleteMan(n3);

		}
		if (n1 == 2){
			Field_officier *fo = dynamic_cast<Field_officier*>(d.get2Commander().getStaff().findMan(n2));
			m = fo->getDivision()->findMan(n3)->clone();
			fo->getDivision()->deleteMan(n3);

		}
	}
	else{
		throw(invalid_argument("Incorrect number"));
	}
	cout << "Enter number of Field_commander of division to transfer soldier in";
	int n4;
	cin >> n4;
	cout << "Enter number of soldier ";
	int n5;
	cin >> n5;
	int iam2;
	if (n1 == 1)
		iam2 = d.get1Commander().getStaff().findMan(n4)->iAm();
	if (n1 == 2)
		iam2 = d.get2Commander().getStaff().findMan(n4)->iAm();
	if (iam2 == 3){
		if (n1 == 1){
			Field_officier *fo = dynamic_cast<Field_officier*>(d.get1Commander().getStaff().findMan(n4));
			fo->getDivision()->addMan(n5, m);

		}
		if (n1 == 2){
			Field_officier *fo = dynamic_cast<Field_officier*>(d.get2Commander().getStaff().findMan(n4));
			fo->getDivision()->addMan(n5, m);

		}
	}
	else{
		throw(invalid_argument("Incorrect number"));
	}
	return 1;
};

int D_find_rank(Descriptor& d){
	cout << "Enter rank" << endl;
	int r;
	cin >> r;
	int k = find_rank(d.get1Commander().getStaff().getTable(), r);
	k += find_rank(d.get2Commander().getStaff().getTable(), r);
	cout << k;
	return 1;
}

int D_split(Descriptor& d){
	cout << "Enter number of commander, 1 or 2" << endl;
	int n1;
	cin >> n1;
	cout << "Enter number of Field_commander of division to split division";
	int n2;
	cin >> n2;
	if (n1 == 1){
		Field_officier *fo = dynamic_cast<Field_officier*>(d.get1Commander().getStaff().findMan(n2));
		fo->getDivision()->Split(d.get1Commander().getStaff(), d.get1Commander().getStaff().findMan(n2));
	}
	if (n1 == 2){
		Field_officier *fo = dynamic_cast<Field_officier*>(d.get2Commander().getStaff().findMan(n2));
		fo->getDivision()->Split(d.get2Commander().getStaff(), d.get2Commander().getStaff().findMan(n2));
	}
	return 1;
}